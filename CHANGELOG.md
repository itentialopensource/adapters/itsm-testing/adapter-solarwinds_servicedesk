
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:29PM

See merge request itentialopensource/adapters/adapter-solarwinds_servicedesk!20

---

## 0.4.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-solarwinds_servicedesk!18

---

## 0.4.2 [08-15-2024]

* Changes made at 2024.08.14_18:42PM

See merge request itentialopensource/adapters/adapter-solarwinds_servicedesk!17

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_19:55PM

See merge request itentialopensource/adapters/adapter-solarwinds_servicedesk!16

---

## 0.4.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!15

---

## 0.3.5 [03-27-2024]

* Changes made at 2024.03.27_14:04PM

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!14

---

## 0.3.4 [03-18-2024]

* Update metadata.json

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!13

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_15:05PM

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!12

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_14:46PM

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!11

---

## 0.3.1 [02-28-2024]

* Changes made at 2024.02.28_11:11AM

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!10

---

## 0.3.0 [01-01-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!9

---

## 0.2.0 [05-26-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!1

---

## 0.1.1 [07-12-2021]

- Initial Commit

See commit 1487e41

---
