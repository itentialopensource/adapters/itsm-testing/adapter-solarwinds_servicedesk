## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Solarwinds ServiceDesk. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Solarwinds ServiceDesk.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for Solarwinds Service Desk . The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentById(id, callback)</td>
    <td style="padding:15px">Get incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIncidentById(id, body, callback)</td>
    <td style="padding:15px">Update incident with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIncidentById(id, callback)</td>
    <td style="padding:15px">Delete incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidents(callback)</td>
    <td style="padding:15px">List of incidents</td>
    <td style="padding:15px">{base_path}/{version}/incidents.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIncident(body, callback)</td>
    <td style="padding:15px">Create new incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProblemById(id, callback)</td>
    <td style="padding:15px">Get problem</td>
    <td style="padding:15px">{base_path}/{version}/problems/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProblemById(id, body, callback)</td>
    <td style="padding:15px">Update problem with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/problems/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProblemById(id, callback)</td>
    <td style="padding:15px">Delete problem</td>
    <td style="padding:15px">{base_path}/{version}/problems/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProblems(callback)</td>
    <td style="padding:15px">List of problems</td>
    <td style="padding:15px">{base_path}/{version}/problems.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProblem(body, callback)</td>
    <td style="padding:15px">Create new problem</td>
    <td style="padding:15px">{base_path}/{version}/problems.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeById(id, callback)</td>
    <td style="padding:15px">Get change</td>
    <td style="padding:15px">{base_path}/{version}/changes/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateChangeById(id, body, callback)</td>
    <td style="padding:15px">Update change with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/changes/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteChangeById(id, callback)</td>
    <td style="padding:15px">Delete change</td>
    <td style="padding:15px">{base_path}/{version}/changes/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanges(callback)</td>
    <td style="padding:15px">List of changes</td>
    <td style="padding:15px">{base_path}/{version}/changes.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChange(body, callback)</td>
    <td style="padding:15px">Create new change</td>
    <td style="padding:15px">{base_path}/{version}/changes.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeCatalogById(id, callback)</td>
    <td style="padding:15px">Get change catalog</td>
    <td style="padding:15px">{base_path}/{version}/change_catalogs/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateChangeCatalogById(id, body, callback)</td>
    <td style="padding:15px">Update change catalog with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/change_catalogs/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteChangeCatalogById(id, callback)</td>
    <td style="padding:15px">Delete change catalog</td>
    <td style="padding:15px">{base_path}/{version}/change_catalogs/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChangeCatalogs(callback)</td>
    <td style="padding:15px">List of change catalogs</td>
    <td style="padding:15px">{base_path}/{version}/change_catalogs.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChangeCatalog(body, callback)</td>
    <td style="padding:15px">Create new change catalog</td>
    <td style="padding:15px">{base_path}/{version}/change_catalogs.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReleaseById(id, callback)</td>
    <td style="padding:15px">Get release</td>
    <td style="padding:15px">{base_path}/{version}/releases/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateReleaseById(id, body, callback)</td>
    <td style="padding:15px">Update release with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/releases/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReleaseById(id, callback)</td>
    <td style="padding:15px">Delete release</td>
    <td style="padding:15px">{base_path}/{version}/releases/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelease(callback)</td>
    <td style="padding:15px">List of release</td>
    <td style="padding:15px">{base_path}/{version}/releases.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRelease(body, callback)</td>
    <td style="padding:15px">Create new release</td>
    <td style="padding:15px">{base_path}/{version}/releases.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSolutionById(id, callback)</td>
    <td style="padding:15px">Get solution</td>
    <td style="padding:15px">{base_path}/{version}/solutions/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSolutionById(id, body, callback)</td>
    <td style="padding:15px">Update solution with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/solutions/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSolutionById(id, callback)</td>
    <td style="padding:15px">Delete solution</td>
    <td style="padding:15px">{base_path}/{version}/solutions/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSolutions(callback)</td>
    <td style="padding:15px">List of solutions</td>
    <td style="padding:15px">{base_path}/{version}/solutions.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSolution(body, callback)</td>
    <td style="padding:15px">Create new solution</td>
    <td style="padding:15px">{base_path}/{version}/solutions.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCatalogItemById(id, callback)</td>
    <td style="padding:15px">Get catalog item</td>
    <td style="padding:15px">{base_path}/{version}/catalog_items/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCatalogItemById(id, body, callback)</td>
    <td style="padding:15px">Update catalog item with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/catalog_items/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCatalogItemById(id, callback)</td>
    <td style="padding:15px">Delete catalog item</td>
    <td style="padding:15px">{base_path}/{version}/catalog_items/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCatalogItems(callback)</td>
    <td style="padding:15px">List of catalog items</td>
    <td style="padding:15px">{base_path}/{version}/catalog_items.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCatalogItem(body, callback)</td>
    <td style="padding:15px">Create new catalog item</td>
    <td style="padding:15px">{base_path}/{version}/catalog_items.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceRequest(id, body, callback)</td>
    <td style="padding:15px">Request a Service</td>
    <td style="padding:15px">{base_path}/{version}/catalog_items/{pathv1}/service_requests.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigurationItemById(id, callback)</td>
    <td style="padding:15px">Get configuration item</td>
    <td style="padding:15px">{base_path}/{version}/configuration_items/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfigurationItemById(id, body, callback)</td>
    <td style="padding:15px">Update configuration item with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/configuration_items/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigurationItemById(id, callback)</td>
    <td style="padding:15px">Delete configurationitem</td>
    <td style="padding:15px">{base_path}/{version}/configuration_items/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigurationItems(callback)</td>
    <td style="padding:15px">List of configuration items</td>
    <td style="padding:15px">{base_path}/{version}/configuration_items.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConfigurationItem(body, callback)</td>
    <td style="padding:15px">Create new configurationitem</td>
    <td style="padding:15px">{base_path}/{version}/configuration_items.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appendDependentAssets(id, body, callback)</td>
    <td style="padding:15px">Append multiple dependent assets</td>
    <td style="padding:15px">{base_path}/{version}/configuration_items/{pathv1}/append_multiple_dependent_assets.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssetLink(body, callback)</td>
    <td style="padding:15px">Delete dpendency by id</td>
    <td style="padding:15px">{base_path}/{version}/asset_links/delete_asset_link_by_id.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserById(id, callback)</td>
    <td style="padding:15px">Get user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUserById(id, body, callback)</td>
    <td style="padding:15px">Update user with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUserById(id, callback)</td>
    <td style="padding:15px">Delete user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">List of Users</td>
    <td style="padding:15px">{base_path}/{version}/users.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(body, callback)</td>
    <td style="padding:15px">Create new user</td>
    <td style="padding:15px">{base_path}/{version}/users.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSiteById(id, callback)</td>
    <td style="padding:15px">Get site</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteById(id, body, callback)</td>
    <td style="padding:15px">Update site with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteById(id, callback)</td>
    <td style="padding:15px">Delete site</td>
    <td style="padding:15px">{base_path}/{version}/sites/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSites(callback)</td>
    <td style="padding:15px">List of Sites</td>
    <td style="padding:15px">{base_path}/{version}/sites.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSite(body, callback)</td>
    <td style="padding:15px">Create new site</td>
    <td style="padding:15px">{base_path}/{version}/sites.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDepartmentById(id, callback)</td>
    <td style="padding:15px">Get department</td>
    <td style="padding:15px">{base_path}/{version}/departments/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDepartmentById(id, body, callback)</td>
    <td style="padding:15px">Update department with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/departments/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDepartmentById(id, callback)</td>
    <td style="padding:15px">Delete department</td>
    <td style="padding:15px">{base_path}/{version}/departments/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDepartments(callback)</td>
    <td style="padding:15px">List of Departments</td>
    <td style="padding:15px">{base_path}/{version}/departments.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDepartment(body, callback)</td>
    <td style="padding:15px">Create new department</td>
    <td style="padding:15px">{base_path}/{version}/departments.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleById(id, callback)</td>
    <td style="padding:15px">Get role</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRoleById(id, body, callback)</td>
    <td style="padding:15px">Update role with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoleById(id, callback)</td>
    <td style="padding:15px">Delete role</td>
    <td style="padding:15px">{base_path}/{version}/roles/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(callback)</td>
    <td style="padding:15px">List of roles</td>
    <td style="padding:15px">{base_path}/{version}/roles.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRole(body, callback)</td>
    <td style="padding:15px">Create new role</td>
    <td style="padding:15px">{base_path}/{version}/roles.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupById(id, callback)</td>
    <td style="padding:15px">Get group</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroupById(id, body, callback)</td>
    <td style="padding:15px">Update group with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroupById(id, callback)</td>
    <td style="padding:15px">Delete group</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(callback)</td>
    <td style="padding:15px">List of groups</td>
    <td style="padding:15px">{base_path}/{version}/groups.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(body, callback)</td>
    <td style="padding:15px">Create new group</td>
    <td style="padding:15px">{base_path}/{version}/groups.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategoryById(id, callback)</td>
    <td style="padding:15px">Get category</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCategoryById(id, body, callback)</td>
    <td style="padding:15px">Update category with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCategoryById(id, callback)</td>
    <td style="padding:15px">Delete category</td>
    <td style="padding:15px">{base_path}/{version}/categories/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategories(callback)</td>
    <td style="padding:15px">List of categories</td>
    <td style="padding:15px">{base_path}/{version}/categories.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCategory(body, callback)</td>
    <td style="padding:15px">Create new category</td>
    <td style="padding:15px">{base_path}/{version}/categories.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHardwareById(id, callback)</td>
    <td style="padding:15px">Get hardware</td>
    <td style="padding:15px">{base_path}/{version}/hardwares/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateHardwareById(id, body, callback)</td>
    <td style="padding:15px">Update hardware with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/hardwares/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHardwareById(id, callback)</td>
    <td style="padding:15px">Delete hardware</td>
    <td style="padding:15px">{base_path}/{version}/hardwares/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHardwares(callback)</td>
    <td style="padding:15px">List of hardwares</td>
    <td style="padding:15px">{base_path}/{version}/hardwares.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createHardware(body, callback)</td>
    <td style="padding:15px">Create new hardware</td>
    <td style="padding:15px">{base_path}/{version}/hardwares.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWarranties(id, callback)</td>
    <td style="padding:15px">List of warranties</td>
    <td style="padding:15px">{base_path}/{version}/hardwares/{pathv1}/warranties.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWarranty(id, body, callback)</td>
    <td style="padding:15px">Create new Warranty</td>
    <td style="padding:15px">{base_path}/{version}/hardwares/{pathv1}/warranties.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWarrantyById(hardwareId, warrantyId, body, callback)</td>
    <td style="padding:15px">Update warranty with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/hardwares/{pathv1}/warranties/{pathv2}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWarrantyById(hardwareId, warrantyId, callback)</td>
    <td style="padding:15px">Delete warranty</td>
    <td style="padding:15px">{base_path}/{version}/hardwares/{pathv1}/warranties/{pathv2}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMobileById(id, callback)</td>
    <td style="padding:15px">Get mobile device</td>
    <td style="padding:15px">{base_path}/{version}/mobiles/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMobileById(id, body, callback)</td>
    <td style="padding:15px">Update mobile device with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/mobiles/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMobileById(id, callback)</td>
    <td style="padding:15px">Delete mobile device</td>
    <td style="padding:15px">{base_path}/{version}/mobiles/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMobiles(callback)</td>
    <td style="padding:15px">List of mobile devices</td>
    <td style="padding:15px">{base_path}/{version}/mobiles.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMobile(body, callback)</td>
    <td style="padding:15px">Create new mobile device</td>
    <td style="padding:15px">{base_path}/{version}/mobiles.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetById(id, callback)</td>
    <td style="padding:15px">Get other asset</td>
    <td style="padding:15px">{base_path}/{version}/other_assets/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAssetById(id, body, callback)</td>
    <td style="padding:15px">Update other asset with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/other_assets/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssetById(id, callback)</td>
    <td style="padding:15px">Delete other asset</td>
    <td style="padding:15px">{base_path}/{version}/other_assets/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssets(callback)</td>
    <td style="padding:15px">List of other assets</td>
    <td style="padding:15px">{base_path}/{version}/other_assets.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAsset(body, callback)</td>
    <td style="padding:15px">Create new asset</td>
    <td style="padding:15px">{base_path}/{version}/other_assets.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwareById(id, callback)</td>
    <td style="padding:15px">Get software</td>
    <td style="padding:15px">{base_path}/{version}/softwares/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwares(callback)</td>
    <td style="padding:15px">List of softwares</td>
    <td style="padding:15px">{base_path}/{version}/softwares.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrinterById(id, callback)</td>
    <td style="padding:15px">Get printer</td>
    <td style="padding:15px">{base_path}/{version}/printers/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePrinterById(id, body, callback)</td>
    <td style="padding:15px">Update printer with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/printers/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPrinters(callback)</td>
    <td style="padding:15px">List of printers</td>
    <td style="padding:15px">{base_path}/{version}/printers.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContractById(id, callback)</td>
    <td style="padding:15px">Get contract</td>
    <td style="padding:15px">{base_path}/{version}/contracts/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateContractById(id, body, callback)</td>
    <td style="padding:15px">Update contract with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/contracts/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteContractById(id, callback)</td>
    <td style="padding:15px">Delete contract</td>
    <td style="padding:15px">{base_path}/{version}/contracts/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContracts(callback)</td>
    <td style="padding:15px">List of contracts</td>
    <td style="padding:15px">{base_path}/{version}/contracts.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createContract(body, callback)</td>
    <td style="padding:15px">Create new contract</td>
    <td style="padding:15px">{base_path}/{version}/contracts.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createItem(id, body, callback)</td>
    <td style="padding:15px">Create new contract's Item</td>
    <td style="padding:15px">{base_path}/{version}/contracts/{pathv1}/items.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateItemById(contractId, itemId, body, callback)</td>
    <td style="padding:15px">Update contract's item with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/contracts/{pathv1}/items/{pathv2}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteitemById(contractId, itemId, callback)</td>
    <td style="padding:15px">Delete contract's item</td>
    <td style="padding:15px">{base_path}/{version}/contracts/{pathv1}/items/{pathv2}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchaseOrderById(id, callback)</td>
    <td style="padding:15px">Get purchase order</td>
    <td style="padding:15px">{base_path}/{version}/purchase_orders/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePurchaseOrderById(id, body, callback)</td>
    <td style="padding:15px">Update purchase order with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/purchase_orders/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePurchaseOrderById(id, callback)</td>
    <td style="padding:15px">Delete purchase order</td>
    <td style="padding:15px">{base_path}/{version}/purchase_orders/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCPurchaseOrders(callback)</td>
    <td style="padding:15px">List of purchase orders</td>
    <td style="padding:15px">{base_path}/{version}/purchase_orders.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPurchaseOrder(body, callback)</td>
    <td style="padding:15px">Create new purchase order</td>
    <td style="padding:15px">{base_path}/{version}/purchase_orders.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorById(id, callback)</td>
    <td style="padding:15px">Get vendor</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVendorById(id, body, callback)</td>
    <td style="padding:15px">Update vendor with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVendorById(id, callback)</td>
    <td style="padding:15px">Delete vendor</td>
    <td style="padding:15px">{base_path}/{version}/vendors/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendors(callback)</td>
    <td style="padding:15px">List of vendors</td>
    <td style="padding:15px">{base_path}/{version}/vendors.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVendor(body, callback)</td>
    <td style="padding:15px">Create new vendor</td>
    <td style="padding:15px">{base_path}/{version}/vendors.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTask(id, objectType, body, callback)</td>
    <td style="padding:15px">Create new Task</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/tasks.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTaskById(id, objectType, taskId, body, callback)</td>
    <td style="padding:15px">Update task with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/tasks/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTaskById(id, objectType, taskId, callback)</td>
    <td style="padding:15px">Delete task</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/tasks/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createComment(id, objectType, body, callback)</td>
    <td style="padding:15px">Create new Comment</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/comments.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCommentById(id, objectType, commentId, body, callback)</td>
    <td style="padding:15px">Update comment with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/comments/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCommentById(id, objectType, commentId, callback)</td>
    <td style="padding:15px">Delete comment</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/comments/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeTracks(id, objectType, callback)</td>
    <td style="padding:15px">List of time tracks</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/time_tracks.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTimeTrack(id, objectType, body, callback)</td>
    <td style="padding:15px">Create new time track</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/time_tracks.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTimeTrackyById(id, objectType, timeTrackId, body, callback)</td>
    <td style="padding:15px">Update time track with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/time_tracks/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTimeTrackyById(id, objectType, timeTrackId, callback)</td>
    <td style="padding:15px">Delete time track</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/time_tracks/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPurchase(id, objectType, body, callback)</td>
    <td style="padding:15px">Create new purchase</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/purchases.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePurchaseById(id, objectType, purchaseId, body, callback)</td>
    <td style="padding:15px">Update purchase with specified fields</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/purchases/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePurchaseById(id, objectType, purchaseId, callback)</td>
    <td style="padding:15px">Delete purchase</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/purchases/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createMembership(groupId, userIds, callback)</td>
    <td style="padding:15px">Create new Membership. Memberships are used to relate a user to a group.
Example: curl -H "X-Samana</td>
    <td style="padding:15px">{base_path}/{version}/memberships.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMemebershipById(id, callback)</td>
    <td style="padding:15px">Delete membership.
Example: curl -H "X-Samanage-Authorization: Bearer TOKEN" -H 'Accept: applicatio</td>
    <td style="padding:15px">{base_path}/{version}/memberships/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAudits(callback)</td>
    <td style="padding:15px">List of audits</td>
    <td style="padding:15px">{base_path}/{version}/audits.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditById(id, objectType, callback)</td>
    <td style="padding:15px">Get audits of a specific object. Example: curl -X GET "https://api.samanage.com/incidents/1/audits"</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}/{pathv2}/audits.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRisks(callback)</td>
    <td style="padding:15px">List of risks</td>
    <td style="padding:15px">{base_path}/{version}/risks.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAttachment(callback)</td>
    <td style="padding:15px">Create a new attachment:  curl -H 'X-Samanage-Authorization: Bearer TOKEN'  -F 'file[attachable_typ</td>
    <td style="padding:15px">{base_path}/{version}/attachments.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
