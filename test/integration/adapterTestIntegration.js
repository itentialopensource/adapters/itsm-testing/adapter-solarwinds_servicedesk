/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-solarwinds_servicedesk',
      type: 'SolarwindsServiceDesk',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const SolarwindsServiceDesk = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] SolarwindsServiceDesk Adapter Test', () => {
  describe('SolarwindsServiceDesk Class Tests', () => {
    const a = new SolarwindsServiceDesk(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-solarwinds_servicedesk-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-solarwinds_servicedesk-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const incidentCreateIncidentBodyParam = {
      incident: {
        name: 'Incident Name',
        site_id: null,
        department_id: null,
        description: 'description',
        state_id: null,
        assignee: {},
        assignee_id: null,
        priority: null,
        requester: {},
        category: {},
        subcategory: {},
        due_at: 'Jan 01,2025',
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incidents: null,
        solutions: null,
        changes: null,
        problems: null,
        releases: null,
        configuration_item_ids: [
          null
        ],
        cc: [
          'john.doe@email.com'
        ]
      }
    };
    describe('#createIncident - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createIncident(incidentCreateIncidentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.incident);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incident', 'createIncident', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidents((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incident', 'getIncidents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const incidentId = 'fakedata';
    const incidentUpdateIncidentByIdBodyParam = {
      incident: {
        name: 'Incident Name',
        site_id: null,
        department_id: null,
        description: 'description',
        state_id: null,
        assignee: {},
        assignee_id: null,
        priority: null,
        requester: {},
        category: {},
        subcategory: {},
        due_at: 'Jan 01,2025',
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incidents: null,
        solutions: null,
        changes: null,
        problems: null,
        releases: null,
        configuration_item_ids: [
          null
        ],
        cc: [
          'john.doe@email.com'
        ]
      }
    };
    describe('#updateIncidentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateIncidentById(incidentId, incidentUpdateIncidentByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incident', 'updateIncidentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIncidentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIncidentById(incidentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.incident);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incident', 'getIncidentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const problemCreateProblemBodyParam = {
      problem: {
        name: 'Problem Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'New',
        requester: {},
        assignee: {},
        priority: 'High',
        root_cause: 'Root cause description',
        symptoms: 'Symptoms description',
        workaround: 'workaround description',
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incident_ids: [
          null
        ],
        itsm_change_ids: [
          null
        ],
        configuration_item_ids: [
          null
        ]
      }
    };
    describe('#createProblem - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createProblem(problemCreateProblemBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.problem);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Problem', 'createProblem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProblems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProblems((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Problem', 'getProblems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const problemId = 'fakedata';
    const problemUpdateProblemByIdBodyParam = {
      problem: {
        name: 'Problem Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'New',
        requester: {},
        assignee: {},
        priority: 'High',
        root_cause: 'Root cause description',
        symptoms: 'Symptoms description',
        workaround: 'workaround description',
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incident_ids: [
          null
        ],
        itsm_change_ids: [
          null
        ],
        configuration_item_ids: [
          null
        ]
      }
    };
    describe('#updateProblemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateProblemById(problemId, problemUpdateProblemByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Problem', 'updateProblemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProblemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProblemById(problemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.problem);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Problem', 'getProblemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeCreateChangeBodyParam = {
      change: {
        name: 'Change Name',
        change_type: 1,
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'New',
        requester: {},
        assignee: {},
        priority: 'High',
        change_plan: 'change plan',
        rollback_plan: 'rollback plan',
        test_plan: 'test plan',
        planned_start_at: '2025-01-01 00:00',
        planned_end_at: '2025-01-01 05:00',
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incidents: null,
        problems: null,
        releases: null,
        configuration_item_ids: [
          null
        ]
      }
    };
    describe('#createChange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createChange(changeCreateChangeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.change);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Change', 'createChange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChanges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChanges((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Change', 'getChanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeId = 'fakedata';
    const changeUpdateChangeByIdBodyParam = {
      change: {
        name: 'Change Name',
        change_type: 1,
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'New',
        requester: {},
        assignee: {},
        priority: 'High',
        change_plan: 'change plan',
        rollback_plan: 'rollback plan',
        test_plan: 'test plan',
        planned_start_at: '2025-01-01 00:00',
        planned_end_at: '2025-01-01 05:00',
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incidents: null,
        problems: null,
        releases: null,
        configuration_item_ids: [
          null
        ]
      }
    };
    describe('#updateChangeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateChangeById(changeId, changeUpdateChangeByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Change', 'updateChangeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChangeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChangeById(changeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.change);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Change', 'getChangeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeCatalogCreateChangeCatalogBodyParam = {
      change_catalog: {
        name: 'Change Name',
        change_type: 1,
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'Approved',
        default_assignee_id: null,
        priority: 'High',
        change_plan: 'change plan',
        rollback_plan: 'rollback plan',
        test_plan: 'test plan',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        show_in_portal: true
      }
    };
    describe('#createChangeCatalog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createChangeCatalog(changeCatalogCreateChangeCatalogBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.change_catalog);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeCatalog', 'createChangeCatalog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChangeCatalogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChangeCatalogs((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeCatalog', 'getChangeCatalogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const changeCatalogId = 'fakedata';
    const changeCatalogUpdateChangeCatalogByIdBodyParam = {
      change_catalog: {
        name: 'Change Name',
        change_type: 1,
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'Approved',
        default_assignee_id: null,
        priority: 'High',
        change_plan: 'change plan',
        rollback_plan: 'rollback plan',
        test_plan: 'test plan',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        show_in_portal: true
      }
    };
    describe('#updateChangeCatalogById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateChangeCatalogById(changeCatalogId, changeCatalogUpdateChangeCatalogByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeCatalog', 'updateChangeCatalogById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChangeCatalogById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChangeCatalogById(changeCatalogId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.change_catalog);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeCatalog', 'getChangeCatalogById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const releaseCreateReleaseBodyParam = {
      release: {
        name: 'Release Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'New',
        requester: {},
        assignee: {},
        priority: 'High',
        plan: 'plan description',
        build: 'build description',
        deploy: 'deploy description',
        planned_start_at: '2025-01-01 00:00',
        planned_end_at: '2025-01-01 05:00',
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        itsm_change_ids: [
          null
        ],
        configuration_item_ids: [
          null
        ],
        approval_levels_attributes: null
      }
    };
    describe('#createRelease - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRelease(releaseCreateReleaseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.release);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'createRelease', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRelease - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRelease((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'getRelease', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const releaseId = 'fakedata';
    const releaseUpdateReleaseByIdBodyParam = {
      release: {
        name: 'Release Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        state: 'New',
        requester: {},
        assignee: {},
        priority: 'High',
        plan: 'plan description',
        build: 'build description',
        deploy: 'deploy description',
        planned_start_at: '2025-01-01 00:00',
        planned_end_at: '2025-01-01 05:00',
        add_to_tag_list: 'tag1',
        remove_from_tag_list: 'tag2',
        tag_list: 'tag1',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        itsm_change_ids: [
          null
        ],
        configuration_item_ids: [
          null
        ],
        approval_levels_attributes: null
      }
    };
    describe('#updateReleaseById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateReleaseById(releaseId, releaseUpdateReleaseByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'updateReleaseById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReleaseById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReleaseById(releaseId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.release);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'getReleaseById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const solutionCreateSolutionBodyParam = {
      solution: {
        name: 'Solution Name',
        site_id: null,
        department_id: null,
        description: 'description',
        state: 'Approved',
        category: {},
        subcategory: {},
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incident_ids: [
          null
        ]
      }
    };
    describe('#createSolution - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSolution(solutionCreateSolutionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.solution);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Solution', 'createSolution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSolutions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSolutions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Solution', 'getSolutions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const solutionId = 'fakedata';
    const solutionUpdateSolutionByIdBodyParam = {
      solution: {
        name: 'Solution Name',
        site_id: null,
        department_id: null,
        description: 'description',
        state: 'Approved',
        category: {},
        subcategory: {},
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incident_ids: [
          null
        ]
      }
    };
    describe('#updateSolutionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSolutionById(solutionId, solutionUpdateSolutionByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Solution', 'updateSolutionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSolutionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSolutionById(solutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.solution);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Solution', 'getSolutionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const catalogItemCreateCatalogItemBodyParam = {
      catalog_item: {
        name: 'CI Name',
        site_id: null,
        department_id: null,
        description: 'description',
        state: 'Approved',
        default_assignee_id: null,
        category: {},
        subcategory: {},
        expected_delivery_time: '1 day',
        currency: 'USD',
        price: '1',
        show_price: null,
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2'
      }
    };
    describe('#createCatalogItem - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCatalogItem(catalogItemCreateCatalogItemBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.catalog_item);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogItem', 'createCatalogItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCatalogItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCatalogItems((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogItem', 'getCatalogItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const catalogItemId = 'fakedata';
    const catalogItemUpdateCatalogItemByIdBodyParam = {
      catalog_item: {
        name: 'CI Name',
        site_id: null,
        department_id: null,
        description: 'description',
        state: 'Approved',
        default_assignee_id: null,
        category: {},
        subcategory: {},
        expected_delivery_time: '1 day',
        currency: 'USD',
        price: '1',
        show_price: null,
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2'
      }
    };
    describe('#updateCatalogItemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCatalogItemById(catalogItemId, catalogItemUpdateCatalogItemByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogItem', 'updateCatalogItemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCatalogItemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCatalogItemById(catalogItemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.catalog_item);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogItem', 'getCatalogItemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceRequestId = 'fakedata';
    const serviceRequestCreateServiceRequestBodyParam = {
      incident: {
        site_id: null,
        department_id: null,
        requester_name: 'john.doe@email.com',
        priority: 'High',
        due_at: 'Jan 01,2025',
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2',
        request_variables_attributes: null
      }
    };
    describe('#createServiceRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createServiceRequest(serviceRequestId, serviceRequestCreateServiceRequestBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.incident);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceRequest', 'createServiceRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationItemCreateConfigurationItemBodyParam = {
      configuration_item: {
        type: {},
        type_id: null,
        name: 'CI Name',
        description: 'description',
        asset_tag: 'Configuration Item Tag',
        site_id: null,
        department_id: null,
        state: 'Active',
        manager: {},
        user: {},
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incident_ids: [
          null
        ],
        itsm_change_ids: [
          null
        ],
        problem_ids: [
          null
        ],
        purchase_order_ids: [
          null
        ],
        release_ids: [
          null
        ],
        configuration_item_ids: [
          null
        ]
      }
    };
    describe('#createConfigurationItem - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createConfigurationItem(configurationItemCreateConfigurationItemBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.configuration_item);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationItem', 'createConfigurationItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationItemDeleteAssetLinkBodyParam = {
      assetLinkId: null,
      sourceId: null,
      sourceType: 'ConfigurationItem'
    };
    describe('#deleteAssetLink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAssetLink(configurationItemDeleteAssetLinkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-solarwinds_servicedesk-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationItem', 'deleteAssetLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigurationItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigurationItems((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationItem', 'getConfigurationItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationItemId = 'fakedata';
    const configurationItemUpdateConfigurationItemByIdBodyParam = {
      configuration_item: {
        type: {},
        type_id: null,
        name: 'CI Name',
        description: 'description',
        asset_tag: 'Configuration Item Tag',
        site_id: null,
        department_id: null,
        state: 'Active',
        manager: {},
        user: {},
        add_to_tag_list: 'tag1, tag2',
        remove_from_tag_list: 'tag3',
        tag_list: 'tag1, tag2',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        incident_ids: [
          null
        ],
        itsm_change_ids: [
          null
        ],
        problem_ids: [
          null
        ],
        purchase_order_ids: [
          null
        ],
        release_ids: [
          null
        ],
        configuration_item_ids: [
          null
        ]
      }
    };
    describe('#updateConfigurationItemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateConfigurationItemById(configurationItemId, configurationItemUpdateConfigurationItemByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationItem', 'updateConfigurationItemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConfigurationItemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfigurationItemById(configurationItemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.configuration_item);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationItem', 'getConfigurationItemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configurationItemAppendDependentAssetsBodyParam = {};
    describe('#appendDependentAssets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appendDependentAssets(configurationItemId, configurationItemAppendDependentAssetsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-solarwinds_servicedesk-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationItem', 'appendDependentAssets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userCreateUserBodyParam = {
      user: {
        name: 'John Doe',
        title: 'Support Agent',
        email: 'john.doe@email.com',
        disabled: false,
        phone: '+10000000',
        mobile_phone: '+10000000',
        role: {},
        site: {},
        department: {},
        reports_to: null,
        custom_fields_values: {
          custom_fields_value: null
        }
      }
    };
    describe('#createUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createUser(userCreateUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'createUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userId = 'fakedata';
    const userUpdateUserByIdBodyParam = {
      user: {
        name: 'John Doe',
        title: 'Support Agent',
        email: 'john.doe@email.com',
        disabled: false,
        phone: '+10000000',
        mobile_phone: '+10000000',
        role: {},
        site: {},
        department: {},
        reports_to: null,
        custom_fields_values: {
          custom_fields_value: null
        }
      }
    };
    describe('#updateUserById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUserById(userId, userUpdateUserByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'updateUserById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserById(userId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'getUserById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteCreateSiteBodyParam = {
      site: {
        name: 'Austin TX, USA',
        location: 'AUS',
        description: 'Description Austin TX, USA',
        time_zone: 'Texas',
        language: 'en',
        manager_id: null,
        default_assignee_id: null,
        business_record: {
          id: null
        }
      }
    };
    describe('#createSite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSite(siteCreateSiteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.site);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'createSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSites((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const siteId = 'fakedata';
    const siteUpdateSiteByIdBodyParam = {
      site: {
        name: 'Austin TX, USA',
        location: 'AUS',
        description: 'Description Austin TX, USA',
        time_zone: 'Texas',
        language: 'en',
        manager_id: null,
        default_assignee_id: null,
        business_record: {
          id: null
        }
      }
    };
    describe('#updateSiteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSiteById(siteId, siteUpdateSiteByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'updateSiteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSiteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSiteById(siteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.site);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'getSiteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const departmentCreateDepartmentBodyParam = {
      department: {
        name: 'Support',
        description: 'Support Department'
      }
    };
    describe('#createDepartment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDepartment(departmentCreateDepartmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.department);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Department', 'createDepartment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDepartments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDepartments((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Department', 'getDepartments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const departmentId = 'fakedata';
    const departmentUpdateDepartmentByIdBodyParam = {
      department: {
        name: 'Support',
        description: 'Support Department'
      }
    };
    describe('#updateDepartmentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDepartmentById(departmentId, departmentUpdateDepartmentByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Department', 'updateDepartmentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDepartmentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDepartmentById(departmentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.department);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Department', 'getDepartmentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleCreateRoleBodyParam = {
      role: {
        name: 'Support Agent',
        description: 'Support Agent role',
        portal: true
      }
    };
    describe('#createRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRole(roleCreateRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.role);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'createRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'getRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const roleId = 'fakedata';
    const roleUpdateRoleByIdBodyParam = {
      role: {
        name: 'Support Agent',
        description: 'Support Agent role',
        portal: true
      }
    };
    describe('#updateRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateRoleById(roleId, roleUpdateRoleByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'updateRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoleById(roleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.role);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'getRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupCreateGroupBodyParam = {
      group: {
        name: 'Group Name',
        description: 'Group Description',
        superviser_id: null,
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null
      }
    };
    describe('#createGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGroup(groupCreateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.group);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'createGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'getGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupId = 'fakedata';
    const groupUpdateGroupByIdBodyParam = {
      group: {
        name: 'Group Name',
        description: 'Group Description',
        superviser_id: null,
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null
      }
    };
    describe('#updateGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroupById(groupId, groupUpdateGroupByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'updateGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupById(groupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.group);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'getGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const categoryCreateCategoryBodyParam = {
      category: {
        name: 'Equipment',
        parent: {
          name: 'Facilities'
        },
        default_assignee_id: null,
        default_tags: 'equipment'
      }
    };
    describe('#createCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCategory(categoryCreateCategoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.category);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Category', 'createCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCategories((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Category', 'getCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const categoryId = 'fakedata';
    const categoryUpdateCategoryByIdBodyParam = {
      category: {
        name: 'Equipment',
        parent: {
          name: 'Facilities'
        },
        default_assignee_id: null,
        default_tags: 'equipment'
      }
    };
    describe('#updateCategoryById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCategoryById(categoryId, categoryUpdateCategoryByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Category', 'updateCategoryById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCategoryById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCategoryById(categoryId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.category);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Category', 'getCategoryById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hardwareCreateHardwareBodyParam = {
      hardware: {
        name: 'Hardware Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        category: {},
        ip_address: '0.0.0.0',
        external_ip: '0.0.0.0',
        status: {},
        technical_contact: {},
        owner: {},
        notes: 'Hardware notes',
        barcode_encoding_format: 'Code1A',
        cpu: 'hardware_cpu',
        memory: null,
        swap: 'MB',
        domain: 'domain description',
        operating_system: 'Windows 10',
        active_directory: 'Workgroup of the OS',
        address: 'Site 1, Building 1, Department 1',
        longitude: '0.000',
        latitude: '0.000',
        product_number: null,
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        bio: {
          manufacturer: 'Dell',
          ssn: null,
          model: 'model description',
          version: null,
          bios_date: '2020-01-01 00:00'
        },
        tag: 'hardware tag',
        asset_tag: null
      }
    };
    describe('#createHardware - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createHardware(hardwareCreateHardwareBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.hardware);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'createHardware', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hardwareId = 'fakedata';
    const hardwareCreateWarrantyBodyParam = {
      warranty: {
        service: 'Warranty service description',
        provider: 'SolarWinds',
        start_date: 'Jan 01, 2020',
        end_date: 'Jan 01, 2030',
        status: 'Active'
      }
    };
    describe('#createWarranty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createWarranty(hardwareId, hardwareCreateWarrantyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.warranty);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'createWarranty', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHardwares - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHardwares((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'getHardwares', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hardwareHardwareId = 'fakedata';
    const hardwareWarrantyId = 'fakedata';
    const hardwareUpdateWarrantyByIdBodyParam = {
      warranty: {
        service: 'Warranty service description',
        provider: 'SolarWinds',
        start_date: 'Jan 01, 2020',
        end_date: 'Jan 01, 2030',
        status: 'Active'
      }
    };
    describe('#updateWarrantyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWarrantyById(hardwareHardwareId, hardwareWarrantyId, hardwareUpdateWarrantyByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'updateWarrantyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hardwareUpdateHardwareByIdBodyParam = {
      hardware: {
        name: 'Hardware Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        category: {},
        ip_address: '0.0.0.0',
        external_ip: '0.0.0.0',
        status: {},
        technical_contact: {},
        owner: {},
        notes: 'Hardware notes',
        barcode_encoding_format: 'Code1A',
        cpu: 'hardware_cpu',
        memory: null,
        swap: 'MB',
        domain: 'domain description',
        operating_system: 'Windows 10',
        active_directory: 'Workgroup of the OS',
        address: 'Site 1, Building 1, Department 1',
        longitude: '0.000',
        latitude: '0.000',
        product_number: null,
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        bio: {
          manufacturer: 'Dell',
          ssn: null,
          model: 'model description',
          version: null,
          bios_date: '2020-01-01 00:00'
        },
        tag: 'hardware tag',
        asset_tag: null
      }
    };
    describe('#updateHardwareById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateHardwareById(hardwareId, hardwareUpdateHardwareByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'updateHardwareById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHardwareById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHardwareById(hardwareId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.hardware);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'getHardwareById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWarranties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWarranties(hardwareId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'getWarranties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mobileDeviceCreateMobileBodyParam = {
      mobile: {
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        status: {},
        device_type: 'Mobile Device',
        manufacturer: 'Apple',
        model: null,
        company_issued: true,
        serial_number: null,
        imei: null,
        user: {},
        technical_contact: {},
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null
      }
    };
    describe('#createMobile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMobile(mobileDeviceCreateMobileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.mobile);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MobileDevice', 'createMobile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMobiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMobiles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MobileDevice', 'getMobiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mobileDeviceId = 'fakedata';
    const mobileDeviceUpdateMobileByIdBodyParam = {
      mobile: {
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        status: {},
        device_type: 'Mobile Device',
        manufacturer: 'Apple',
        model: null,
        company_issued: true,
        serial_number: null,
        imei: null,
        user: {},
        technical_contact: {},
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null
      }
    };
    describe('#updateMobileById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMobileById(mobileDeviceId, mobileDeviceUpdateMobileByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MobileDevice', 'updateMobileById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMobileById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMobileById(mobileDeviceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.mobile);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MobileDevice', 'getMobileById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const otherAssetCreateAssetBodyParam = {
      other_asset: {
        name: 'Asset Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        asset_id: null,
        asset_type: {
          name: 'Printer'
        },
        status: {
          name: 'Operational'
        },
        manufacturer: 'Apple',
        ip_address: '0.0.0.0',
        model: null,
        serial_number: null,
        user: {},
        owner: {},
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null
      }
    };
    describe('#createAsset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAsset(otherAssetCreateAssetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.other_asset);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OtherAsset', 'createAsset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssets((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OtherAsset', 'getAssets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const otherAssetId = 'fakedata';
    const otherAssetUpdateAssetByIdBodyParam = {
      other_asset: {
        name: 'Asset Name',
        description: 'description',
        site: {},
        department: {},
        site_id: null,
        department_id: null,
        asset_id: null,
        asset_type: {
          name: 'Printer'
        },
        status: {
          name: 'Operational'
        },
        manufacturer: 'Apple',
        ip_address: '0.0.0.0',
        model: null,
        serial_number: null,
        user: {},
        owner: {},
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null
      }
    };
    describe('#updateAssetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAssetById(otherAssetId, otherAssetUpdateAssetByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OtherAsset', 'updateAssetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssetById(otherAssetId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.other_asset);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OtherAsset', 'getAssetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSoftwares - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSoftwares((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Software', 'getSoftwares', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const softwareId = 'fakedata';
    describe('#getSoftwareById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSoftwareById(softwareId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.software);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Software', 'getSoftwareById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrinters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrinters((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Printer', 'getPrinters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const printerId = 'fakedata';
    const printerUpdatePrinterByIdBodyParam = {
      printer: {
        site_id: null,
        department_id: null,
        technical_contact: {},
        address: 'address description'
      }
    };
    describe('#updatePrinterById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePrinterById(printerId, printerUpdatePrinterByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Printer', 'updatePrinterById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPrinterById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPrinterById(printerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.printer);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Printer', 'getPrinterById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contractCreateContractBodyParam = {
      contract: {
        type: 'Software License',
        manufacturer_name: 'Apple',
        name: 'Contract Name',
        note: 'Contract Note',
        status: 'Active',
        site: {},
        department: {},
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        start_date: '2020-01-01T00:00:00-00:00',
        end_date: '2030-01-01T00:00:00-00:00',
        tag_list: 'tag1, tag2'
      }
    };
    describe('#createContract - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createContract(contractCreateContractBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.contract);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'createContract', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contractId = 'fakedata';
    const contractCreateItemBodyParam = {
      item: {
        name: 'Item Name',
        version: null,
        qty: null,
        tag: 'Item tag'
      }
    };
    describe('#createItem - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createItem(contractId, contractCreateItemBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.item);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'createItem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContracts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContracts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'getContracts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contractContractId = 'fakedata';
    const contractItemId = 'fakedata';
    const contractUpdateItemByIdBodyParam = {
      item: {
        name: 'Item Name',
        version: null,
        qty: null,
        tag: 'Item tag'
      }
    };
    describe('#updateItemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateItemById(contractContractId, contractItemId, contractUpdateItemByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'updateItemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const contractUpdateContractByIdBodyParam = {
      contract: {
        type: 'Software License',
        manufacturer_name: 'Apple',
        name: 'Contract Name',
        note: 'Contract Note',
        status: 'Active',
        site: {},
        department: {},
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        start_date: '2020-01-01T00:00:00-00:00',
        end_date: '2030-01-01T00:00:00-00:00',
        tag_list: 'tag1, tag2'
      }
    };
    describe('#updateContractById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateContractById(contractId, contractUpdateContractByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'updateContractById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContractById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContractById(contractId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.contract);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'getContractById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseOrderCreatePurchaseOrderBodyParam = {
      purchase_order: {
        name: 'Purchase order Name',
        buyer_id: null,
        order_date: 'Jan 01, 2025',
        due_date: 'Jan 01, 2030',
        site: {},
        department: {},
        state: 'Approved',
        requester: {},
        recurrence: 'Monthly',
        total_cost: null,
        currency: 'USD',
        notes: 'Purchase notes',
        vendor: {},
        billing_address: 'Billing address',
        shiping_address: 'Shiping address',
        payment_terms: 'Purchase terms',
        terms_conditions: 'Terms conditions',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        approval_levels_attributes: null
      }
    };
    describe('#createPurchaseOrder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPurchaseOrder(purchaseOrderCreatePurchaseOrderBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.purchase_order);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseOrder', 'createPurchaseOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCPurchaseOrders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCPurchaseOrders((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseOrder', 'getCPurchaseOrders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseOrderId = 'fakedata';
    const purchaseOrderUpdatePurchaseOrderByIdBodyParam = {
      purchase_order: {
        name: 'Purchase order Name',
        buyer_id: null,
        order_date: 'Jan 01, 2025',
        due_date: 'Jan 01, 2030',
        site: {},
        department: {},
        state: 'Approved',
        requester: {},
        recurrence: 'Monthly',
        total_cost: null,
        currency: 'USD',
        notes: 'Purchase notes',
        vendor: {},
        billing_address: 'Billing address',
        shiping_address: 'Shiping address',
        payment_terms: 'Purchase terms',
        terms_conditions: 'Terms conditions',
        custom_fields_values: {
          custom_fields_value: null
        },
        custom_fields_values_attributes: null,
        approval_levels_attributes: null
      }
    };
    describe('#updatePurchaseOrderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePurchaseOrderById(purchaseOrderId, purchaseOrderUpdatePurchaseOrderByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseOrder', 'updatePurchaseOrderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPurchaseOrderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPurchaseOrderById(purchaseOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.purchase_order);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseOrder', 'getPurchaseOrderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorCreateVendorBodyParam = {
      vendor: {
        name: 'SolarWinds',
        vendor_type: {
          name: 'General Business Vendor'
        },
        url: 'www.solarwinds.com',
        contact_name: 'Support',
        contact_email: 'support@solarwinds.com',
        contact_phone: '+000000000',
        note: 'Notes',
        address: 'Address description',
        city: 'Cary',
        state: 'NC',
        zip: 'USA 11111',
        telephone: '+000000000'
      }
    };
    describe('#createVendor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVendor(vendorCreateVendorBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.vendor);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'createVendor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVendors((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'getVendors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vendorId = 'fakedata';
    const vendorUpdateVendorByIdBodyParam = {
      vendor: {
        name: 'SolarWinds',
        vendor_type: {
          name: 'General Business Vendor'
        },
        url: 'www.solarwinds.com',
        contact_name: 'Support',
        contact_email: 'support@solarwinds.com',
        contact_phone: '+000000000',
        note: 'Notes',
        address: 'Address description',
        city: 'Cary',
        state: 'NC',
        zip: 'USA 11111',
        telephone: '+000000000'
      }
    };
    describe('#updateVendorById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateVendorById(vendorId, vendorUpdateVendorByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'updateVendorById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVendorById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVendorById(vendorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.vendor);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'getVendorById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskId = 'fakedata';
    const taskObjectType = 'fakedata';
    const taskCreateTaskBodyParam = {
      task: {
        name: 'Task Name',
        assignee: {},
        due_at: 'Jan 01, 2030',
        is_complete: false
      }
    };
    describe('#createTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTask(taskId, taskObjectType, taskCreateTaskBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.task);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'createTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const taskTaskId = 'fakedata';
    const taskUpdateTaskByIdBodyParam = {
      task: {
        name: 'Task Name',
        assignee: {},
        due_at: 'Jan 01, 2030',
        is_complete: true
      }
    };
    describe('#updateTaskById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTaskById(taskId, taskObjectType, taskTaskId, taskUpdateTaskByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'updateTaskById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commentId = 'fakedata';
    const commentObjectType = 'fakedata';
    const commentCreateCommentBodyParam = {
      comment: {
        body: 'Comment body',
        is_private: null
      }
    };
    describe('#createComment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createComment(commentId, commentObjectType, commentCreateCommentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.comment);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comment', 'createComment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const commentCommentId = 'fakedata';
    const commentUpdateCommentByIdBodyParam = {
      comment: {
        body: 'Comment body',
        is_private: null
      }
    };
    describe('#updateCommentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCommentById(commentId, commentObjectType, commentCommentId, commentUpdateCommentByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comment', 'updateCommentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeTrackId = 'fakedata';
    const timeTrackObjectType = 'fakedata';
    const timeTrackCreateTimeTrackBodyParam = {
      time_track: {
        name: 'Time Track Name',
        minutes_parsed: '2h'
      }
    };
    describe('#createTimeTrack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTimeTrack(timeTrackId, timeTrackObjectType, timeTrackCreateTimeTrackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.time_track);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeTrack', 'createTimeTrack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTimeTracks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTimeTracks(timeTrackId, timeTrackObjectType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeTrack', 'getTimeTracks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const timeTrackTimeTrackId = 'fakedata';
    const timeTrackUpdateTimeTrackyByIdBodyParam = {
      time_track: {
        name: 'Time Track Name',
        minutes_parsed: '2h'
      }
    };
    describe('#updateTimeTrackyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTimeTrackyById(timeTrackId, timeTrackObjectType, timeTrackTimeTrackId, timeTrackUpdateTimeTrackyByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeTrack', 'updateTimeTrackyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchaseId = 'fakedata';
    const purchaseObjectType = 'fakedata';
    const purchaseCreatePurchaseBodyParam = {
      purchase: {
        number: null,
        date: 'Jan 01, 2025',
        recurrence: 'Monthly',
        total_cost: null,
        currency: 'USD',
        notes: 'Purchase notes',
        vendor: {}
      }
    };
    describe('#createPurchase - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPurchase(purchaseId, purchaseObjectType, purchaseCreatePurchaseBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.purchase);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchase', 'createPurchase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const purchasePurchaseId = 'fakedata';
    const purchaseUpdatePurchaseByIdBodyParam = {
      purchase: {
        number: null,
        date: 'Jan 01, 2025',
        recurrence: 'Monthly',
        total_cost: null,
        currency: 'USD',
        notes: 'Purchase notes',
        vendor: {}
      }
    };
    describe('#updatePurchaseById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePurchaseById(purchaseId, purchaseObjectType, purchasePurchaseId, purchaseUpdatePurchaseByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchase', 'updatePurchaseById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const membershipGroupId = 'fakedata';
    const membershipUserIds = 'fakedata';
    describe('#createMembership - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createMembership(membershipGroupId, membershipUserIds, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-solarwinds_servicedesk-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Membership', 'createMembership', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAudits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAudits((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'getAudits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const auditId = 'fakedata';
    const auditObjectType = 'fakedata';
    describe('#getAuditById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuditById(auditId, auditObjectType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Audit', 'getAuditById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRisks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRisks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Risk', 'getRisks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAttachment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createAttachment((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-solarwinds_servicedesk-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Attachment', 'createAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIncidentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIncidentById(incidentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Incident', 'deleteIncidentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProblemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteProblemById(problemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Problem', 'deleteProblemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteChangeById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteChangeById(changeId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Change', 'deleteChangeById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteChangeCatalogById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteChangeCatalogById(changeCatalogId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChangeCatalog', 'deleteChangeCatalogById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReleaseById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteReleaseById(releaseId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'deleteReleaseById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSolutionById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteSolutionById(solutionId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Solution', 'deleteSolutionById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCatalogItemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteCatalogItemById(catalogItemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CatalogItem', 'deleteCatalogItemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConfigurationItemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteConfigurationItemById(configurationItemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigurationItem', 'deleteConfigurationItemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUserById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteUserById(userId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('User', 'deleteUserById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteSiteById(siteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Site', 'deleteSiteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDepartmentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDepartmentById(departmentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Department', 'deleteDepartmentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoleById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteRoleById(roleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Role', 'deleteRoleById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroupById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteGroupById(groupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Group', 'deleteGroupById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCategoryById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteCategoryById(categoryId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Category', 'deleteCategoryById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWarrantyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWarrantyById(hardwareHardwareId, hardwareWarrantyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'deleteWarrantyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHardwareById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteHardwareById(hardwareId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hardware', 'deleteHardwareById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMobileById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteMobileById(mobileDeviceId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MobileDevice', 'deleteMobileById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAssetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAssetById(otherAssetId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OtherAsset', 'deleteAssetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteitemById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteitemById(contractContractId, contractItemId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'deleteitemById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteContractById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteContractById(contractId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Contract', 'deleteContractById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseOrderById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePurchaseOrderById(purchaseOrderId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PurchaseOrder', 'deletePurchaseOrderById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVendorById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVendorById(vendorId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vendor', 'deleteVendorById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTaskById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTaskById(taskId, taskObjectType, taskTaskId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Task', 'deleteTaskById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCommentById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteCommentById(commentId, commentObjectType, commentCommentId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Comment', 'deleteCommentById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTimeTrackyById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTimeTrackyById(timeTrackId, timeTrackObjectType, timeTrackTimeTrackId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TimeTrack', 'deleteTimeTrackyById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePurchaseById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePurchaseById(purchaseId, purchaseObjectType, purchasePurchaseId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Purchase', 'deletePurchaseById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const membershipId = 'fakedata';
    describe('#deleteMemebershipById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteMemebershipById(membershipId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Membership', 'deleteMemebershipById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
