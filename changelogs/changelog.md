
## 0.2.0 [05-26-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-solarwinds_servicedesk!1

---

## 0.1.1 [07-12-2021]

- Initial Commit

See commit 1487e41

---
