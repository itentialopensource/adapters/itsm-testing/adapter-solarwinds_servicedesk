# Adapter for Solarwinds Service Desk 

Vendor: Solarwinds 
Homepage: https://www.solarwinds.com/

Product: Service Desk 
Product Page: https://www.solarwinds.com/service-desk

## Introduction
We classify Solarwinds ServiceDesk into the ITSM domain as Solarwinds ServiceDesk is a cloud-based solution designed to streamline IT support and operations with features like ticketing, asset management, and workflow automation.

## Why Integrate
The Solarwinds ServiceDesk adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Solarwinds ServiceDesk. With this adapter you have the ability to perform operations on items such as:

- Containers
- Nodes
- Subnets
- Pollers
- Policy

## Additional Product Documentation
The [API documents for Solarwinds ServiceDesk](https://apidoc.samanage.com/)